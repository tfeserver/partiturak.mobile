//import 'react-native-gesture-handler';
import * as React from 'react';
import { View, Text, StatusBar } from 'react-native';
import {useRef, useState, useEffect} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Menu from './src/Menu.js';
import List from './src/List.js';
import Partitura from './src/Partitura.js';
import Intro from './src/Intro.js';
import constants from './src/Constants.js';
import { MaterialIcons } from '@expo/vector-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { MenuProvider } from 'react-native-popup-menu';

const Stack = createStackNavigator();

function MainApp() {

  return (
    <Stack.Navigator screenOptions={{
        headerTintColor: constants.lightText,
            headerStyle: {
                backgroundColor: constants.web_color3,
            },
            headerTintColor: constants.web_color2,
            headerTitleStyle: {
                color:constants.web_color2,
                fontWeight: 'bold',
            },
    }}>
      <Stack.Screen name="Intro" options={ ({navigation}) => ({
          headerTitle:'Partiturak',
          headerLeft: () => (
              <View style={{ justifyContent:"center", height:'100%', flexDirection:'row', alignItems:"center" }}>

                  <MaterialIcons
                  name='menu'
                  style={{ marginRight: 2, padding:15 }}
                  color={ constants.web_color2 }
                  onPress={() => { navigation.navigate('Menu') }}
                  size={20}
                  />
              </View>
          ) 
      })}
      >
            {(props) => <Intro {...props} />}
      </Stack.Screen>

      <Stack.Screen name="List" options={{ headerTitle:'Partiturak' }}>
            {(props) => <List {...props} />}
      </Stack.Screen>

      <Stack.Screen name="Menu" options={{ headerTitle:'Menu' }}>
            {(props) => <Menu {...props} />}
      </Stack.Screen>

      <Stack.Screen name="Partitura">
            {(props) => <Partitura {...props} />}
      </Stack.Screen>
    </Stack.Navigator>
  );
}

export default function App() {
  return (
    <MenuProvider>
    <NavigationContainer>
      <StatusBar hidden />
      <MainApp />
    </NavigationContainer>
    </MenuProvider>
  );
}

