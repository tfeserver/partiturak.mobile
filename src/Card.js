import React from "react";
import { Fragment, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import constants from './Constants.js';
import { AntDesign } from '@expo/vector-icons';


export default ({ props, data }) => {

    const navigation = props.navigation;
    const route = props.route;


    const tags = props.tags;
    const item = props.item;

    return (
        <TouchableOpacity style={ styles.content }
            onPress={() => {
                if(!item.loading) {
                    navigation.push('Partitura', { item });
                }

            }}>
            <View style={ styles.card }>
                <View style={styles.header}>
                    <Text style={ styles.title } numberOfLines={1}>{ item.name }</Text>
                    <View style={styles.tags}>
                        { item.tags.map((id) => {
                            let tag = tags[id];
                            if(!tag.isInstrument)
                            {
                                return ( <Text numberOfLines={1}  key={item.id+'-'+tag.id} style={ styles.tag }>{ tag.name }</Text>);
                            }
                        })}
                    </View>
                </View>
                <View style={styles.imageContainer}>
                    <Image resizeMode="contain" style={styles.image} source={{ uri: 'https://partiturak.eus/'+(item?.mainPdf || '').replace('pdf','jpg') }} />
                </View>
                <View style={ styles.footer }>
                    <Text numberOfLines={1} style={styles.date}>{ (item?.date?.date || '').replace(/(\d+)\-(\d+)\-(\d+).*/,'$1ko $2ren $3a') }</Text>
                    <View style={styles.viewsContainer}>
                        <Text numberOfLines={1} style={styles.views}>   { item.views } </Text>
                        <AntDesign
                          name='eyeo'
                          size={15}
                          color={'#000'}
                        />
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    content: {
        width: '100%',
        aspectRation: 1,
        maxWidth: 1000,
        padding:5,
        flexDirection: 'row',
        alignItems: 'stretch',
        justifyContent: 'center',
    },
    card: {
        flexDirection: 'column',
        width: '100%',
        height:'100%',
        textColor:'#fff',
        borderWidth: 1,
        overflow:'hidden',
        borderColor: constants.web_color7,
        backgroundColor: constants.web_color7,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        height: '100%',
    },
    header: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: constants.web_color7,
    },
    tags: {
        width:'100%',
        flexDirection: 'row',
        marginTop:0,
        gap:5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        color:constants.darkText,
        letterSpacing:1.2,
        paddingTop:10,
        fontWeight:'bold',
        fontSize:14,
        textAlign: 'center',
        width:'100%',
    },
    tag: {
        color:constants.darkText,
        fontSize:11,
        paddingTop:5,
        paddingBottom:2,
        paddingLeft:5,
        paddingRight:5,
        textAlign: 'center',
    },
    imageContainer: {
        width: '100%',
        aspectRatio:1.3,
        overflow:'hidden'
    },
    image: {
        width: '135%',
        marginLeft:'-18%',
        marginTop:'-28%',
        opacity:0.8,
        height: 500,
    },
    footer:
    {
        flexDirection: 'row',
        padding:10,
        width: '100%',
        alignItems: 'center',
        borderTopWidth: 1,
        borderTopColor: '#e4e8ec',
        justifyContent: 'space-between',
    },
    viewsContainer:{
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    date: {
        fontSize: 10,
        letterSpacing:0.3,
    },
    views:
    {
        fontSize: 10,
        alignItems: 'center',
        opacity:0.8,
    },
    viewsImage:
    {
        opacity:0.8,
        width: 18,
        height: 18,
    }
});
