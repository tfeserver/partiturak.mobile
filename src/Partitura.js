import React from "react";
import { Dimensions, Image, TouchableOpacity, Text, ScrollView, View}  from 'react-native';
import constants from './Constants.js';
import ScaledImage from './ScaledImage.js';
import PartituraInside from './PartituraInside.js';
import Player from './Player.js';
import { AntDesign } from '@expo/vector-icons';
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {useRef, useState, useEffect} from 'react';
import {Alert, Share, Button} from 'react-native';
import ExternalLink from './ExternalLink.js';
import * as FileSystem from 'expo-file-system';
import { StorageAccessFramework } from 'expo-file-system';
import { Buffer } from "buffer";
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';

const Tab = createBottomTabNavigator();



export default ({ route , navigation }) => {


    const [pdf, setPdf] = React.useState(null);
    const [uri, setUri] = React.useState(null);
    const [item, setItem] = React.useState({});
    const [isLoading, setIsLoading] = React.useState(true);
    const [downloading, setDownloading] = React.useState(false);

    const downloadFile = async (uri, fileName) => {

        setDownloading(true);
        try
        {
            const response = await fetch(uri)
            const buffer = await response.arrayBuffer();
            const base64Data = Buffer.from(buffer).toString('base64');
            setDownloading(false);


            const permissions = await StorageAccessFramework.requestDirectoryPermissionsAsync();
            if (!permissions.granted) {
                return;
            }

            await StorageAccessFramework.createFileAsync(permissions.directoryUri, fileName, 'application/pdf')
                .then(async(uri) => {
                    await FileSystem.writeAsStringAsync(uri, base64Data, { encoding: FileSystem.EncodingType.Base64 });
                })
        }
        catch(e)
        {
            console.log('error',e);
        }
    };


    useEffect((x) => {
        setItem(route.params.item);
    }, [route]);

    useEffect((x) => {
        fetch('https://partiturak.eus/ikusi/'+encodeURIComponent(item.url));
        setPdf(item.mainPdf);
    }, [item]);

    useEffect((x) => {

        if(!pdf) { return; }
        setIsLoading(true);

        // Increase stats website
        fetch('https://partiturak.eus/ikusi/'+encodeURIComponent(item.url));

        // Check cache
        let fileName = item.name+'.pdf';
        let dest = FileSystem.documentDirectory + pdf.replace(/.*\//,'');

        const filePromise = FileSystem.getInfoAsync(dest);
        filePromise.then(async (fileInfo) => {
            if (!fileInfo.exists) {
                console.log("File does not exist...");
                let dl = FileSystem.createDownloadResumable(
                    'https://partiturak.eus/'+pdf,
                    dest,
                    {}
                );
                let r = await dl.downloadAsync();
                console.log('downloaded!',r);
                setIsLoading(false);
                setUri(r.uri);
            }
            else
            {
                setIsLoading(false);
                console.log('already exists!',fileInfo);
                setUri(fileInfo.uri);
            }
        });

        navigation.setOptions({
              title: item.name,
              headerRight: () => (
                  <View style={{flexDirection:'row', alignItems:'center' }}>

                      <MaterialIcons
                      name='share'
                      style={{ marginRight: 10, padding:15 }}
                      color={ constants.web_color2 }
                      onPress={onShare}
                      size={20}
                      />
                  <TouchableOpacity onPress={ () => {
                      downloadFile('https://partiturak.eus/'+pdf, item.name+'.pdf');
                  }}>
                          <MaterialIcons
                          name={downloading ? 'downloading' : 'download'}
                          style={{ marginRight: 0, padding:15 }}
                          color={ constants.web_color2 }
                          size={20}
                          />
                  </TouchableOpacity>

                      <Menu>
                          <MenuTrigger>
                              <MaterialCommunityIcons
                              name='dots-vertical'
                              style={{ marginRight: 0, padding:15 }}
                              color={ constants.web_color2 }
                              size={20}
                              />
                          </MenuTrigger>
                          <MenuOptions>
                               <MenuOption key={item.name} text={item.name} onSelect={() => { setPdf(item.mainPdf) } }  />
                              { item.pdfs && item.pdfs.map((pdf) => {
                                return (<MenuOption key={item.name+pdf.name} text={pdf.name} onSelect={() => { setPdf(pdf.pdf) } }  />)
                              })}
                          </MenuOptions>
                      </Menu>
                  </View>
              ),
              headerTitleStyle: {
                  fontWeight: 'bold',
              },
        })
    }, [pdf, downloading]);

    const openMenu = async() => {
    };

    const onShare = async () => {
        try {
            const result = await Share.share({
                message:
                'Hemen duzu '+item.name+ '-ko partitura:  https://partiturak.eus/ikusi/'+item.url
            });
            if (result.action === Share.sharedAction) {
            } else if (result.action === Share.dismissedAction) {
            }
        } catch (error) {
            Alert.alert(error.message);
        }
    };

    if(!isLoading && item && item.id) {
        return (
            <View style={{flex:1,width:'100%'}}>
                <View style={{
                    position:'relative',
                    zIndex:98,
                    width:'100%',
                    height:'100%'
                }}>
                { uri && (
                <PartituraInside mainNav={navigation} uri={uri} />
                )}
                </View>

                <View style={{
                    position:'absolute',
                    bottom:0,
                    zIndex:100,
                    width:'100%'
                }}>
                <Player item={item} navigation={navigation} />
                </View>
            </View>
        );
    }
    else
    {
        return (
            <View style={{ height:'100%' , alignItems:'center', justifyContent:'center'}}>
            <Text>Kargatzen...</Text>
            </View>
        );
    }
};
