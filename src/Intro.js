import React from "react";
import { TouchableOpacity, Dimensions, FlatList, StyleSheet, Text, View, ScrollView } from 'react-native';
import {useState, useEffect} from 'react';
import constants from './Constants.js';
import Card from './Card.js';
import ShortcutList from './ShortcutList.js';
import AsyncStorage from '@react-native-async-storage/async-storage';
import globalStyles from './Styles.js';



export default ({ route, navigation }) => {

    const [tags, setTags] = useState([]);

    useEffect((x) => {
    }, []);

    let items = [
        { title: 'Partitura guztiak', dest:'List', conf:  { sort: 0, tag: { id: null, titleName:'Partiturak', name:'Partitura Guztiak'} } },
        { title: 'Erakutsi kategoriak', dest:'Menu', conf:  {  } },
    ];
    return (
        <ScrollView>
            <View style={{ marginTop:15, marginLeft:15, marginRight:15, gap: 15 }}>
            <Text>Aplikazio honetan partitura sorta bat aurkituko duzu, PDF formatuan eta doainik. Materiala txistulari hasiberrientzat zein beteranoentzat izan daiteke, nahi duzun bezala erabil dezakezu.</Text>
            </View>

            <View style={{gap:15, marginLeft:15, marginRight:15, marginTop:30, }}>

            { items.map((item) => {
                return (
                    <ShortcutList dest={item.dest} key={item.title} conf={item.conf} title={item.title} navigation={navigation}/>
                );
                }) }

            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
});

