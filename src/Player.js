import { Dimensions, View, Text, TouchableOpacity}  from 'react-native';
import constants from './Constants.js';
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import {useState, useEffect} from 'react';
import { Audio } from 'expo-av';


export default ({ route , navigation, item }) => {
    const [isPlaying, setIsPlaying] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [audios, setAudios] = useState([]);
    const [pos, setPos] = useState(null);
    const [total, setTotal] = useState(null);
    const [layoutWidth, setLayoutWidth] = useState(10);
    const [barVisible, setBarVisible] = useState(false);
    const [width, setWidth] = useState(100);

    const updateColumns = () => {
        let windowWidth = Dimensions.get('window').width;
        setWidth(windowWidth);
    };


    const playAdv = () => {
        audios.forEach((audio) => {audio.playFromPositionAsync(Math.min(total,pos+5000)); setIsPlaying(true);});
    }
    const playRet = () => {
        audios.forEach((audio) => {audio.playFromPositionAsync(Math.max(0,pos-5000)); setIsPlaying(true);});
    }
    const playPos = (percent) => {
        let pos = total * percent;
        audios.forEach((audio) => {audio.playFromPositionAsync(pos); setIsPlaying(true);});
    };

    const playPause = () =>
    {
        setBarVisible(true);
        if(isPlaying) {
            audios.forEach((audio) => { audio.pauseAsync() });
            setIsPlaying(false);
        }
        else {
            audios.forEach((audio) => {audio.playAsync()});
            setIsPlaying(true);
        }
    };

    useEffect((x) => {
        return () => {
            audios.forEach((audio) => { audio.pauseAsync(); audio.unloadAsync() });
        }
    }, [audios]);

    useEffect(() => {
        const sub = Dimensions.addEventListener('change', updateColumns);
        updateColumns();
        return () => {
            console.log('remove sub player');
            sub?.remove();
        }
    }, [])

    useEffect((x) => {
        let audios_tmp = [];
        
        const _onPlaybackStatusUpdate = (status) => 
        {
            let allLoaded = true;
            audios.forEach((audio) => {
                allLoaded = allLoaded && audio?.status?.isLoaded;
            });
            setPos(status.positionMillis);
            setTotal(status.durationMillis);
            setIsLoading(!allLoaded && status.durationMillis);
        }
        const  initialStatus =  {
            shouldPlay: false,
            rate: 1,
            shouldCorrectPitch: true,
            volume: 0.7,
            isMuted: false,
            isLooping: true
        };
        (async () =>
        {
            try
            {
                for(idx in item.mp3s)
                {
                    let mp3 = item.mp3s[idx];
                    let {sound, status} = await Audio.Sound.createAsync(
                        {uri:('https://partiturak.eus/'+mp3.mp3)},
                        initialStatus,
                        _onPlaybackStatusUpdate,

                    );
                    sound.setProgressUpdateIntervalAsync(1000);
                    audios_tmp.push(sound);
                }
            }
            catch(err)
            {
                console.error('error! ',err);
            }
            setAudios(audios_tmp);
        })();

    }, [item]);

    return (
        <View style={{
            flexDirection:'row',
                justifyContent:'center',
                alignItems:'center',
                padding:2,
                color: constants.web_color2,
        }}>
                { isLoading ?
                    <View style={{ flexDirection:'row', justifyContent:'center', alignItems:'center' }}>
                    <Text style={{
                        color:constants.web_color2
                    }}>Kargatzen...</Text>
                    <MaterialIcons
                    name="downloading"
                    style={{ marginLeft:15, marginRight: 10, padding: 0}}
                    size={30}
                    color={ constants.web_color2 } />
                    </View>
                     :
                    <>
                    <View style={{
                        position:'relative',
                        flexDirection:'column',
                        justifyContent:barVisible ? 'center' : 'flex-end',
                        alignItems: barVisible ? 'center' : 'flex-end',
                        width:'100%',
                        padding:0,
                    }}>
                        <View style={{
                            flexDirection:'row',
                            justifyContent:'center',
                            alignItems:'center',
                            color: constants.web_color2,
                            padding:0,
                            height:70,
                            color: constants.web_color2,
                        }}>
                            { barVisible &&  (<MaterialIcons name="arrow-left" style={{ marginRight: 10, padding:0 }} color={ constants.web_color3 } onPress={ playRet} size={40} />) }
                            <MaterialIcons name={isPlaying ? 'pause-circle': 'play-circle'} style={{ marginRight: 10, width:'10px', padding:0 }} color={ constants.web_color3 } onPress={ playPause} size={barVisible ? 60 : 40} />
                            { barVisible && (<MaterialIcons name="arrow-right" style={{ marginRight: 10, padding:0 }} color={ constants.web_color3 } onPress={ playAdv} size={40} />) }
                        </View>



                        <View style={{
                            zIndex:10,
                            width: Math.min(width*0.9, 600),
                            height:30,
                            marginBottom:10,
                            display:barVisible ? 'block':'none',
                            backgroundColor: constants.web_color3,
                            padding: 2,
                            }}
                        >
                            <View
                                onLayout={(event) => {
                                    setLayoutWidth(event?.nativeEvent?.layout?.width || 10);
                                }}
                                style={{
                                    position:'absolute',
                                    top:2,
                                    left:2,
                                    width: '100%',
                                    height:26,
                                    display:'block',
                                    backgroundColor: constants.web_color2,
                                    padding: 0,
                                    }}
                            >
                                <View style={{
                                    backgroundColor: constants.web_color7,
                                        position:'absolute',
                                        width: (pos/total*100)+'%',
                                        height:'100%',
                                }}>
                                </View>
                                <TouchableOpacity onPress={(evt) => {
                                    playPos(evt.nativeEvent.locationX / layoutWidth);
                                }}>
                                <Text style={{
                                width:'100%',
                                color:constants.web_color3,
                                textAlign:'center',
                                padding:2,
                                fontSize:15
                                }}>
                                { total && (
                                    (Math.ceil(pos / 1000) / 60).toFixed(2) +' / '+ (Math.ceil(total / 1000) / 60).toFixed(2)
                                ) }
                                </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    </>
                }
        </View>);
}

